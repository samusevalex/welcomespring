FROM alpine as builder
RUN apk add openjdk11-jdk
ADD http://gitlab.com/samusevalex/WelcomeSpring/-/archive/master/WelcomeSpring-master.tar .
RUN tar xf *.tar \
   && mv WelcomeSpring-master/* . \
   && ./gradlew bootJar

FROM alpine
RUN apk add openjdk11-jre-headless
COPY --from=builder /build/libs/main.jar .
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "main.jar"]
