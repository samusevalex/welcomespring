package org.example;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.beans.factory.annotation.Value;

@RestController
public class ApiController {

    @Value("${ymlVar}")
    private String appVar;

    @GetMapping()
    public String printVar (){
        return appVar;
    }
}
